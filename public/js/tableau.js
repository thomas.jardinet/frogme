$(document).ready(function () {
    Papa.parse('data/listesolutions.csv', {
        download: true,
        header: true,
        skipEmptyLines: true,
        complete: function (results) {
            var table = $('#myTable').DataTable({
                data: results.data,
                pageLength: 25,
                columns: [
                    {
                        data: null,
                        render: function (data, type, row) {
                            var familles = [
                                row['Zone'], row['Quartier'], row['Ilot']
                            ].filter(Boolean).join(' <strong>/</strong> ');
                            return familles;
                        }
                    },
                    {
                        data: 'Nom',
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var url = row['URL'];
                                return '<a href="' + url + '" target="_blank">' + data + '</a>';
                            }
                            return data;
                        }
                    },
                    {
                        data: 'Description'
                    },
                    {
                        data: 'Type'
                    },
                    {
                        data: null,
                        render: function (data, type, row) {
                            var certifications = ['Secnumcloud', 'HDS', '27001', '27701', '27018:2019', '27017:2015', '20000-1:2018', '50001', 'HDA/HADS', 'SOC 2 Type II', 'SOC 1 Type II', '14001', 'PCI-DSS', '14040', 'SecNumCloud en cours', 'SecNumCloud en projet', 'Utilise un hébergeur SecNumCloud', 'Homologation DINUM', 'ISO 22301', 'CSPN'].filter(function (column) {
                                var value = row[column];
                                return value && value.trim() !== "?" && value.trim() !== "Non";
                            }).join(' <strong>/</strong> ');
                            return certifications;
                        }
                    },
                    {
                        data: null,
                        render: function (data, type, row) {
                            var Reseaux = '';
                            if (row['Twitter']) {
                                Reseaux += ' <a href="' + row['Twitter'] + '" target="_blank"><img class="logo-image" src="images/X-logo-black.png" alt="Twitter"></a>';
                            }

                            if (row['Linkedin']) {
                                Reseaux += ' <a href="' + row['Linkedin'] + '" target="_blank"><img class="logo-image" src="images/LI-In-Bug.png" alt="LinkedIn"></a>';
                            }
                            return Reseaux;
                        }
                    }
                ],
                columnDefs: [
                    {
                        width: '10px',
                        targets: [
                            0,
                            1,
                            2,
                            3,
                            4,
                            5
                        ]
                    }
                ],
                responsive: true,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 0
                },
                language: {
                    "url": "https://cdn.datatables.net/plug-ins/1.13.7/i18n/fr-FR.json"
                }
            });

            var categories = results.data.map(function (row) {
                return [row['Zone'], row['Quartier'], row['Ilot']
            ].filter(Boolean).join(' / ');
            }).filter(function (category, index, self) {
                return category && self.indexOf(category) === index;
            }).sort();

            var select = $('#categorieDropdown');
            select.empty(); 
            //select.append('<option value="">Toutes les catégories</option>'); // Ajouter une option vide pour éviter la sélection automatique

            categories.forEach(function (category) {
                select.append('<option value="' + category + '">' + category + '</option>');
            });

            select.select2({
                //placeholder: "Sélectionner une catégorie",
                allowClear: true,
                closeOnSelect: false,
                multiple: true, // Activer la sélection multiple
                theme: "classic" // Utiliser le thème classique de Select2
            });
            // Ajoutez cette ligne pour définir la valeur de sélection à vide
            select.val(null).trigger('change');

            // Ajouter un gestionnaire d'événement pour filtrer le tableau en fonction de la sélection dans la liste déroulante
            select.on('change', function () {
                var selectedCategories = $(this).val();
                if (selectedCategories && selectedCategories.length > 0) {
                    table.columns(0).search(selectedCategories.map(function(category) {
                        return '^' + category + '$';
                    }).join('|'), true, false).draw();
                } else {
                    table.columns(0).search('').draw();
                }
            });
        }
    });
});
