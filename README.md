## Projet Framagit du site [GrenouilleMoi](https://grenouillemoi.fr)

## Kezako?

Ce projet a pour vocation d'être un annuaire de solutions françaises, que ce soit d'éditeurs logiciels ou de fournisseurs clouds.

Il n'a pas vocation à énumérer ce qui est "souverain" ou non, mais plutôt à énumérer des solutions d'entreprises françaises.

Sont tout de fois exclues les entreprises qui revendent du logiciel "final" étranger, hormis Open Source, et les entreprises pour lesquelles les capitaux sont connues de manière publiques comme étant majoritairement étrangers.

En effet, entre lois extra-territoriales et capacités d’intelligence de pays tierces, mais aussi pour des questions de dépendance juridique et économique, et au regard de la certification SecNumCloud qui a comme critères la part d’actionnariat étranger, ces critères sont retenus pour filtrer les sociétés françaises concernées.

Le scope de ces solutions se veut généraliste, rajouter tous les éditeurs de solutions métiers très spécifiques rendrerait cette liste difficilement lisible, sans compter qu'elles sont souvent connues des entreprises clientes interessées.

L'ambition est plutôt de démontrer qu'on peut monter une très grande partie de son système d'information à l'aide de solutions françaises.

## Et l'open source dans tout ça?

Evidemment, on ne peut qu'encourager l'utilisation de solutions open source. Si ce projet est chez Framagit, ce n'est pas que pour la gratuité ;-) 

Néanmoins, les listings de solutions open source existent, il n'est pas dit que rajouter un énième catalogue fasse sens.

## Je connais une super solution qui n'est pas la! Et il y'a une erreur dans le listing!

Ce "projet" est en soit open source, libre à vous de le reprendre, ou de contribuer, comme indiqué dans le fichier [CONTRIBUTING](CONTRIBUTING.md).

De plus, à date le site est toujours en cours de construction, le rythme de mise à jour devrait atteindre son rythme de croisière d’ici mi-avril 2024.

## Mais pourtant, untel dit qu’il est SecNumCloud! Et vous ne le mettez pas!!

Oui, alors effectivement, seulement voila, seul [le site officiel de l’ANSSI fait foi](https://cyber.gouv.fr/produits-services-qualifies?sort_bef_combine=nom_du_fournisseur_ASC&field_type_service_value%5Bcloud%5D=cloud&categorie_psq=).

Après, un éditeur peut aussi s’héberger chez un hébergeur SecNumCloud, c’est mieux certes, mais ca ne certifie pas la solution automatiquement.

Néanmoins, sachant que la certification peut prendre deux ans, le fait que le fournisseur indique etre en train de la passer est un bon point qu’on peut prendre en compte! 

Quand d'autres souhaitent la passer, mais ne sont pas suffisamment avancé pour être retrouvé sur le site de l'ANSSI comme quoi ils sont en train de la passer.

Du coup, le fichier fait la distinction entre "SecNumCloud", qui est mieux que "SecNumCloud en cours", qui est mieux que "SecNumCloud en projet" ou que "Utilise un hébergeur SecNumCloud".

## L'Ihm pourrait être plus jolie!

J'avoue... Mais pareil, libre à vous de contribuer ;-)

## Mesure d'audience

Un script Matomo est utilisé pour faire une mesure d'audience, dont les métriques remontent vers un serveur personnel située en France.

Matomo a été choisie pour sa gratuité et sa conformité [RGPD](https://fr.matomo.org/faq/is-matomo-analytics-gdpr-compliant/).

Seuls les user agent, ips anonymisés et horaires de passage sont stockés, et ne sont en aucun cas revendues ou repartagées. 

La durée de stockage de ces données est de 6 mois. 

Les mesures d'exemption de consentement disponibles sur le [site de la CNIL pour Matomo](https://www.cnil.fr/sites/cnil/files/atoms/files/matomo_analytics_-_exemption_-_guide_de_configuration.pdf) ont été mis en place.

Néanmoins, vous pouvez cliquer sur la tarte au citron en haut à droite pour désactiver la mesure d'audience.

Si cela vous gêne tout de même, je ne peux que vous conseiller d'utiliser AdBlock sur votre navigateur.

Si malgré toute ma vigilance, un problème subsiste, contactez moi!

## Comment me contacter?

Je suis disponible sur [Twitter](https://twitter.com/ThomasJardinet) et [Linkedin](https://www.linkedin.com/in/thomasjardinet/).